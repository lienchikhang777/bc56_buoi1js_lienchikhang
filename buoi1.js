//bai 1
/**
 * input: so ngay lam viec
 * 
 * progress:
 * khoi tao bien workingDays = 17
 * khoi tao bien salaryPerDay va gan = 100000
 * khoi tao bien salarySum = salaryPerDay * workingDays
 * in salarySum ra man hinh console
 * 
 * output: tong luong
 */

var workingDays = 17;
var salaryPerDay = 100000;
var salarySum = salaryPerDay * workingDays;
console.log("🚀 ~ file: bai1.js:16 ~ salarySum:", salarySum)

//bai2
/**
 * input: 5 so thuc (firstNum, secondNum, thirdNum, fourthNum, fifthNum )
 * 
 * progress: 
 * khoi tao firstNum = 1.2
 * khoi tao secondNum = 5.3
 * khoi tao thirdNum = 7.2
 * khoi tao fourthNum = 4.8
 * khoi tao fifthNum = 3.9
 * 
 * khoi tao bien sum = firstNum + secondNum + thirdNum + fourthNum + fifthNum
 * khoi tao bien avgNum = sum / 5
 * in ra man hinh console avgNum
 * 
 * output: trung binh cong 5 so thuc (avgNum)
 */

var firstNum = 1.2;
var secondNum = 5.3;
var thirdNum = 7.2;
var fourthNum = 4.8;
var fifthNum = 3.9;

var sum = fifthNum + secondNum + thirdNum + fourthNum + fifthNum;
var avgNum = sum / 5;
console.log("🚀 ~ file: bai1.js:44 ~ avgNum:", avgNum);

//bai 3
/**
 * input: so tien USD
 * 
 * progress:
 * khoi tao bien usdPrice = 2
 * khoi tao bien vnPrice = 23500
 * khoi tao bien vnPriceAfter = usdPrice * vnPrice
 * in ra man hinh console vnPriceAfter
 * 
 * output: so tien VND
 */

var usdPrice = 2;
var vnPrice = 23500;
var vnPriceAfter = usdPrice * vnPrice;
console.log("🚀 ~ file: bai1.js:62 ~ vnPriceAfter:", vnPriceAfter)

//bai4
/**
 * input: chieu dai, chieu rong hcn
 * 
 * progress:
 * khoi tao bien length = 5
 * khoi tao bien width = 7
 * khoi tao bien area = length * width
 * khoi tao bien perimeter = (length + width) * 2
 * in ra man hinh console area va perimeter
 * 
 * output: area va perimeter
 */

var length = 5;
var width = 7;
var area = length * width;
var perimeter = (length + width) * 2;
console.log("🚀 ~ file: bai1.js:81 ~ area:", area)
console.log("🚀 ~ file: bai1.js:83 ~ perimeter:", perimeter)

//bai5
/**
 * input: 1 so co 2 ky so
 * 
 * progress:
 * khoi tao bien n = 12
 * khoi tao bien ten = Math.floor(n / 10)
 * khoi tao bien unit = n % 10
 * khoi tao bien sum = ten + unit
 * in ra man hinh console bien sum
 * 
 * output: tong 2 ky so
 */
var n = 12
var ten = Math.floor(n / 10);
var unit = n % 10;
var sum = ten + unit;
console.log("🚀 ~ file: bai1.js:101 ~ sum:", sum)





